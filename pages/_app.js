import React from 'react'
import App, { Container } from 'next/app'

import { Provider } from "react-redux";
import ReduxStore from "../src/store/Store";

// class Layout extends React.Component {
//     render() {
//         const { children } = this.props
//         return <div className='layout'>{children}</div>
//     }
// }

class MyApp extends App {

  render() {
    const { Component, pageProps} = this.props;

    return (
      <Container>
        <Provider store={ReduxStore}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    )
  }
}

export default MyApp
