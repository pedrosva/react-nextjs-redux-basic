import React, { Component } from "react";

import { connect } from "react-redux";

import { addArticle } from "../../src/actions";

class ExampleComponent extends Component {
  constructor() {
    super();
    this.state = {
      articles: [
        { title: "React Redux Tutorial for Beginners", id: 1 },
        { title: "Redux e React: cos'è Redux e come usarlo con React", id: 2 }
      ]
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    event.preventDefault();
  }
  

  render() {
    const { articles } = this.state;
    return <ul>{this.props.article.map(el => <li key={el.id}>{el.title}</li>)}</ul>;
  }
}


function mapStateToProps(state) {
  return {
    article: state.app.articles,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addArticle: article => dispatch(addArticle(article))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExampleComponent);