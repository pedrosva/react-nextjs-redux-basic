
import { ADD_ARTICLE } from "../constants/action-types";

const initialState = {
  articles: [
    { title: "Teste1", id: 1 },
    { title: "Teste2", id: 2 }
  ]
};


function appReducer(state = initialState, action) {

  switch (action.type) {

    case ADD_ARTICLE: {
      return {
        ...state,
        articles: state.articles.concat(action.payload)
      };
    }

    default:
      return state;
  }
}

export default appReducer;